const log = {}

log.install = function (Vue) {
    Vue.prototype.$log = window.console.log
}

export default log
