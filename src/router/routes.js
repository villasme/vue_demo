const routes = [
    { path: '/hello-world', component: () => import(/* webpackChunkName: "hello_world" */ '../components/HelloWorld.vue')},
    { path: '/tab', alias: '/index',component: () => import(/* webpackChunkName: "tab" */ '../components/Tab.vue')},
    { path: '/test-router/:id', component: () => import(/* webpackChunkName: "test_router" */ '../components/TestRouter.vue'),
        beforeEnter: (to, from, next) => {
           window.console.log('路由beforeEnter守卫')
           next()
        }
    },
    {path: '', redirect: '/index'}
]

export default routes
