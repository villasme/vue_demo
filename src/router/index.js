import Vue from 'vue'
import VueRouter from 'vue-router'

import routes from './routes'

Vue.use(VueRouter)

const router = new VueRouter({
    routes
})
router.beforeEach((to, from, next) => {
    window.console.log('全局前置守卫：beforeEach')
    next()
})
router.afterEach(route => {
    window.console.log('全局后置守卫： afterEach', route)
})

export default router
